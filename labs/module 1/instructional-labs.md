# Instructional Course Labs Sample

<img src="images/IDSNlogo.png" width="200" height="200"/>

Provide a brief  Scenario or Overview
or few introductory sentences

## Objectives
After completing this lab, you will be able to:
1. Objective for Exercise 1
1. Objective for Exercise 2
1. etc.


## Exercise 1 : < Title of Exercise 1 >
In this exercise, you will < short sentence describing what’s done in this exercise >.

1. Step 1 details ...

1. Step 2 details with sample screenshot...
![sample screenshot](images/samplescreenshot1.png)

1. Step 3 details if image requires resizing
<img src="images/samplescreenshot1.png" width="450" height="275"/>

1. Step 4 details...

## Exercise 2 : < Title of Exercise 2 >
In this exercise, you will ...

### Task A : < Title of Task A in Ex 2 >

1. Step 1 in Task A in Ex 2

1. Step 2 in Task A in Ex 2

### Task B : < Title of Task B in Ex2 >

1. Step 1 details


1. Step 2 details


## (Optional) Summary / Conclusion / Next Steps
Add ending info here, if required, and rename the title accordingly. Otherwise remove this optional section.

## Author(s)
<h4> [Author1 Name](optional link to profile) <h4/>
<h4> [Author2 Name](optional link to profile) <h4/>

### Other Contributor(s) 
< Contributor 1 Name >, < Contributor 2 Name >, etc.

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| yyyy-mm-dd | 0.1 | changer name | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
